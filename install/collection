#!/bin/bash

## installs aircrafts listed in the indicated collection
## use -a or --allow-nongpl to allow installation of NonGPL aircrafts 
## in Aircraft-nonGPL directory

## USAGE EXAMPLES
##
## install/collection base-legacy ##installs the original base collection
## install/collection all --allow-nongpl
## install/collection iahmcol-favorites ##installs IAHM-COL GPL favorites

# Copyright (C) 2017 IAHM-COL

#This program is free software; you can redistribute it and/or
#modify it under the terms of the GNU General Public License
#as published by the Free Software Foundation; either version 2
#of the License, or (at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program; if not, write to the Free Software
#Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#Handles Getopts for --allow-nongpl (or -a)
OPTS=`getopt -o a --long allow-nongpl -n 'collection' -- "$@"`
if [ $? != 0 ] ; then echo "Failed parsing options." >&2 ; exit 1 ; fi
eval set -- "$OPTS"
NGPL=false
while true; do
    case "$1" in 
    -a | --allow-nongpl ) NGPL=true; shift ;;
    -- ) shift; break ;;
    * ) break ;;
    esac
done

#obtain the aircrafts list in array collection
IFS=$'\r\n' GLOBIGNORE='*' command eval  'collection=($(cat install/collections/$1))'

#updates aircraft 
for aircraft in "${collection[@]}"; do
    git submodule update --init  Aircraft/${aircraft}
done

#updates nonGPL aircrafts if requested
if $NGPL 
then
    #obtain the aircrafts list in array collection
    IFS=$'\r\n' GLOBIGNORE='*' command eval  'collectionnongpl=($(cat install/collections-nongpl/$1))'
    git submodule update --init Aircraft-nonGPL
    cd Aircraft-nonGPL
    #updates aircraft 
    for aircraft in "${collectionnongpl[@]}"; do
	git submodule update --init  ${aircraft}
    done
    cd ..
fi
